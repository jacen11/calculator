package com.example.calc


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigDecimal
import kotlin.math.sqrt


class MainActivity : AppCompatActivity() {


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val resultFromView: TextView = findViewById(R.id.result)

        btn1.setOnClickListener { resultFromView.text = "${resultFromView.text}1" }
        btn2.setOnClickListener { resultFromView.text = "${resultFromView.text}2" }
        btn3.setOnClickListener { resultFromView.text = "${resultFromView.text}3" }
        btn4.setOnClickListener { resultFromView.text = "${resultFromView.text}4" }
        btn5.setOnClickListener { resultFromView.text = "${resultFromView.text}5" }
        btn6.setOnClickListener { resultFromView.text = "${resultFromView.text}6" }
        btn7.setOnClickListener { resultFromView.text = "${resultFromView.text}7" }
        btn8.setOnClickListener { resultFromView.text = "${resultFromView.text}8" }
        btn9.setOnClickListener { resultFromView.text = "${resultFromView.text}9" }
        btn0.setOnClickListener {
            if (resultFromView.text.isNotBlank()) {
                if (resultFromView.text.last() != '/') {
                    resultFromView.text = "${resultFromView.text}0"
                } else {
                    Toast.makeText(this, "Деление на ноль не поддерживается", Toast.LENGTH_SHORT).show()
                }
            } else {
                resultFromView.text = "0"
            }
        }

        btnSquareRoot.setOnClickListener {
            if (resultFromView.text.isEmpty()) {
                Toast.makeText(this, "Пусто", Toast.LENGTH_SHORT).show()
            }
            if (isDigital(resultFromView.text.toString())) {
                resultFromView.text = sqrt(resultFromView.text.toString().toDouble()).toString()
            } else {
                Toast.makeText(this, "Можно применить только на положительном числе", Toast.LENGTH_SHORT).show()
            }
        }

        btnSwitch.setOnClickListener {
            if (resultFromView.text.isNotEmpty()) {
                if (resultFromView.text.first() == '-') {
                    resultFromView.text = resultFromView.text.substring(1)
                } else {
                    resultFromView.text = "-${resultFromView.text}"
                }
            }
        }

        multBtn.setOnClickListener {
            if (resultFromView.text.isNotEmpty() && validation(resultFromView.text.toString())) resultFromView.text =
                "${resultFromView.text}*"
        }
        minusBtn.setOnClickListener {
            if (validation(resultFromView.text.toString())) resultFromView.text = "${resultFromView.text}-"
        }
        plusBtn.setOnClickListener {
            if (validation(resultFromView.text.toString())) resultFromView.text = "${resultFromView.text}+"
        }
        devBtn.setOnClickListener {
            if (resultFromView.text.isNotEmpty() && validation(resultFromView.text.toString())) resultFromView.text =
                "${resultFromView.text}/"
        }

        btnDot.setOnClickListener {
            if (validation(resultFromView.text.toString()) && checkDot(resultFromView.text.toString())) {
                resultFromView.text = "${resultFromView.text}."
            }
        }
        btnClear.setOnClickListener { resultFromView.text = "" }

        btnBackspace.setOnClickListener {
            resultFromView.text =
                if (resultFromView.text.isNotEmpty()) resultFromView.text.substring(
                    0,
                    resultFromView.text.length - 1
                ) else ""
        }


    }

    private fun checkDot(str: String): Boolean {
        val textWithoutDigital: String = str.replace(Regex("\\d"), "")
        if (textWithoutDigital.isEmpty() || textWithoutDigital.last() != '.') {
            return true
        }
        return false
    }

    private fun isDigital(str: String) = str.isNotEmpty() && str.replace(Regex("\\d|\\."), "").isEmpty()

    fun answerPreview(view: View) {

        val result: TextView = findViewById(R.id.result)
        val txtAnswerPreview: TextView = findViewById(R.id.txtAnswerPreview)
        txtAnswerPreview.text = "qwe"

        if (result.text.first().equals('-') || result.text.first().equals('+')) {
            result.text = "0${result.text}"
        }

        val resultPreview: String = toStartCalculation(result.text.toString()).toString()
        if (resultPreview.endsWith(".0")) {
            txtAnswerPreview.text = resultPreview.substringBeforeLast(".0")
        } else {
            txtAnswerPreview.text = resultPreview
        }
    }

    private fun validation(str: String): Boolean {

        if (str.isNotEmpty() && str.last() in "/*-+.") {
            Toast.makeText(this, "Нельзя повторять знаки", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }


    fun onClickEqualsBtn(view: View) {
        val result: TextView = findViewById(R.id.result)

        if (result.text.first().equals('-') || result.text.first().equals('+')) {
            result.text = "0${result.text}"
        }

        val resultPreview: String = toStartCalculation(result.text.toString()).toString()
        if (resultPreview.endsWith(".00") || resultPreview.endsWith(".0")) {
            result.text = resultPreview.substringBeforeLast(".0")
        } else {
            result.text = resultPreview
        }

    }

    private fun toStartCalculation(str: String): BigDecimal =
        if (str.takeLast(1).contains(Regex("[^\\d]"))) sumAndSub(str.dropLast(1)) else sumAndSub(str)

    private fun sumAndSub(str: String): BigDecimal {
        val numbers = str.split(Regex("[+,-]"))
            .map { multAndDiv(it) }
        val sign = str.split(Regex("[^+||^-]")).filter { it.isNotEmpty() }
        var result = numbers[0]

        sign.forEachIndexed { index, it ->
            when (it) {
                "+" -> result += numbers[index + 1]
                "-" -> result -= numbers[index + 1]
            }
        }
        return result
    }

    private fun multAndDiv(string: String): BigDecimal {
        val numbers = string.split(Regex("[*,/]")).map { BigDecimal.valueOf(it.toDouble()) }
        val sign = string.split(Regex("[^/||^*]")).filter { it.isNotEmpty() }
        var result = numbers[0]

        sign.forEachIndexed { index, it ->
            when (it) {
                "*" -> result *= numbers[index + 1]
                "/" -> result /= numbers[index + 1]//}
            }
        }
        return result
    }

}
